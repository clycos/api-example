const oracledb = require('oracledb');
const dbConfig = '{template: {user:'', password:'', connectString:'(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=)(PORT = 1521))(CONNECT_DATA=(SERVICE_NAME=)))',},};';

(async function () {
  let conn; // Declared here for scoping purposes

  try {
    conn = await oracledb.getConnection(dbConfig.template);

    console.log('Connected to database');

    let result = await conn.execute(
      `SELECT *
         FROM dual
      `,
      [], // no binds
      {
        outFormat: oracledb.OBJECT,
      }
    );

    console.log('Query executed');
    console.log(result.rows);
  } catch (err) {
    console.log('Error in processing', err);
  } finally {
    if (conn) {
      // conn assignment worked, need to close
      try {
        await conn.close();

        console.log('Connection closed');
      } catch (err) {
        console.log('Error closing connection', err);
      }
    }
  }
})();
