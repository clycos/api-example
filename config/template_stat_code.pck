CREATE OR REPLACE PACKAGE template_states_pkg AS
PROCEDURE crud (p_method IN char, p_state_code IN varchar2, p_user_id IN varchar2, p_status OUT varchar2);
END template_states_pkg;
/

CREATE OR REPLACE PACKAGE BODY template_states_pkg AS
PROCEDURE crud (p_method IN char, p_state_code IN varchar2, p_user_id IN varchar2, p_status OUT varchar2) IS
-- use the following to force uppercase
v_method CHAR(1) := UPPER(p_method);
v_user_id VARCHAR2(8) := UPPER(p_user_id);
v_state_code template_states.state_code%TYPE := UPPER(p_state_code);

-- other variables
v_count NUMBER(2) := 0;
v_timestamp VARCHAR2(50) := v_user_id||' on '||TO_CHAR(sysdate,'DD-MON-YYYY HH24:MI');

BEGIN
-- END (RETURN) IF NOT A VALID CHOICE
IF v_method NOT IN ('C','U','D') THEN p_status := 'Invalid Option'; RETURN; END IF;

--------------------------------------------------------------------------------------------------------------
IF v_method = 'C' THEN
INSERT INTO template_states (state_code, user_id)
VALUES (v_state_code, v_user_id);
COMMIT;
p_status := v_state_code || ' created by '||v_timestamp;
END IF;
--------------------------------------------------------------------------------------------------------------
IF v_method = 'D' THEN
--UPPER NOT NEEDED ON FOLLOWING state_code DUE TO FOREIGN KEY ON VALIDATION TABLE CONTAINING UPPERCASE VALUES
SELECT 1 INTO v_count FROM template_states WHERE state_code = v_state_code;
IF v_count > 0
THEN
--UPPER NOT NEEDED ON FOLLOWING state_code DUE TO FOREIGN KEY ON VALIDATION TABLE CONTAINING UPPERCASE VALUES
DELETE FROM template_states WHERE state_code = v_state_code;
p_status := v_state_code || ' deleted by '||v_timestamp;
COMMIT;
END IF;
END IF;
--------------------------------------------------------------------------------------------------------------
IF v_method = 'U' THEN
--UPPER NOT NEEDED ON FOLLOWING state_code DUE TO FOREIGN KEY ON VALIDATION TABLE CONTAINING UPPERCASE VALUES
--following select duplicates another SELECT above - consider putting both under one if statement
SELECT 1 INTO v_count FROM template_states WHERE state_code = v_state_code;
IF v_count > 0
THEN
UPDATE template_states
   SET activity_date = sysdate,
       user_id = v_user_id;
p_status := v_state_code || ' updated by '||v_timestamp;
COMMIT;
END IF;
END IF;

EXCEPTION
      WHEN DUP_VAL_ON_INDEX
      THEN p_status := v_state_code||' already in table';
      WHEN NO_DATA_FOUND
      THEN p_status := v_state_code||' does not exist in table';
END crud;
END template_states_pkg;
