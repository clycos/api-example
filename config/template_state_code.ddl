drop table TEMPLATE_STATES;
create table TEMPLATE_STATES
 (state_code VARCHAR2(3) NOT NULL,
  user_id   VARCHAR2(30) DEFAULT user NOT NULL,
  activity_date DATE DEFAULT sysdate NOT NULL,
  PRIMARY KEY (state_code),
  FOREIGN KEY (state_code) REFERENCES template_state_val(state_code)
 );
