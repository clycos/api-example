const express = require('express');
const app = express();
const oracledb = require('oracledb');

const webServerConfig = require('./config/web-server.js');
const dbConfig = require('./config/database.js').template;

app.use(express.json()); //for raw json
app.use(express.urlencoded()); //for x-www-form-urlencoded

app.listen(webServerConfig.port, () => {
  console.log(`http://localhost:${webServerConfig.port}/api/version --get`);
  console.log(`http://localhost:${webServerConfig.port}/api/state --get post`);
  console.log(`http://localhost:${webServerConfig.port}/api/state/MI --get delete put`);
  console.log(`http://localhost:${webServerConfig.port}/api/state/pkg --post`);
});

app.route('/api/version').get(async function (req, res, next) {
  res.send('Version 1.0.0');
});

// GET API - basic select command
app.route('/api/state').get(async function (req, res) {
  let connection, returnResult;
  try {
    const sql = `SELECT state_CODE, state_desc from template_state_val`;
    const binds = {};
    const options = {
      outFormat: oracledb.OBJECT,
      resultSet: true,
    };

    connection = await oracledb.getConnection(dbConfig);
    const result = await connection.execute(sql, binds, options);

    const resultResultSet = result.resultSet;
    let resultRows = await resultResultSet.getRows(60);
    await resultResultSet.close();

    returnResult = resultRows;
  } catch (err) {
    res.send(error_format(err));
  } finally {
    if (connection) {
      await connection.close();
    }
    res.send(returnResult);
  }
});

// GET API - basic select command using a parameter
app.route('/api/state/:state_code').get(async function (req, res) {
  let connection, returnResult;
  try {
    const sql = `SELECT state_CODE, state_desc from template_state_val WHERE state_code = :state_code`;
    const binds = {
      state_code: { type: oracledb.STRING, dir: oracledb.BIND_IN, val: req.params.state_code },
    };
    const options = {
      outFormat: oracledb.OBJECT,
      resultSet: true,
    };

    connection = await oracledb.getConnection(dbConfig);
    const result = await connection.execute(sql, binds, options);

    const resultResultSet = result.resultSet;
    let resultRows = await resultResultSet.getRows(10000);
    await resultResultSet.close();

    returnResult = resultRows;
  } catch (err) {
    res.send(error_format(err));
  } finally {
    if (connection) {
      await connection.close();
    }
    res.send(returnResult);
  }
});

// POST API - basic insert
// try it in the browser - it won't work because this is a .put request
// need to use POSTMAN, Thunder Client(vscode extension), or CURL(command line - recommend using git bash)
// use following JSON for testing: {"state_code":"UT", "user_id":"bueller"}
// for test, run twice using same parms to see how the output differs
// exercise (if no already added) - set error if oracle error - still returns a 200
app.route('/api/state').post(async function (req, res) {
  let connection, result, sql, options, binds, resultRows;
  try {
    sql = `INSERT INTO template_states (state_code, user_id)
           VALUES (UPPER(:state_code), UPPER(:user_id))`;
    binds = {
      state_code: {
        type: oracledb.STRING,
        dir: oracledb.BIND_IN,
        val: req.body.state_code,
      },
      user_id: { type: oracledb.STRING, dir: oracledb.BIND_IN, val: req.body.user_id },
    };
    options = { autoCommit: true };

    connection = await oracledb.getConnection(dbConfig);
    result = await connection.execute(sql, binds, options);
    await connection.commit;
  } catch (err) {
    res.send(error_format(err));
  } finally {
    if (connection) {
      await connection.close();
    }
    res.send(result);
  }
});

// PUT API - basic update using the following JSON
// need to use POSTMAN, Thunder Client(vscode extension), or CURL(command line - recommend using git bash)
// {"user_id":"dadude"}
// discuss pro/con of putting some of the updates in a column vs a trigger
// CREATE OR REPLACE TRIGGER TEMPLATE_TEST_TI
// BEFORE INSERT OR UPDATE ON template_states
// FOR EACH ROW
// BEGIN :NEW.activity_date := sysdate;
// END;
app.route('/api/state/:state_code').put(async function (req, res) {
  let connection, result, sql, options, binds;
  try {
    sql = `UPDATE template_states
              SET user_id   = :user_id,
                  activity_date = sysdate
            WHERE state_code = :state_code`;
    binds = {
      state_code: { type: oracledb.STRING, dir: oracledb.BIND_IN, val: req.params.state_code },
      user_id: { type: oracledb.STRING, dir: oracledb.BIND_IN, val: req.body.user_id },
    };
    options = { autoCommit: true };

    connection = await oracledb.getConnection(dbConfig);

    result = await connection.execute(sql, binds, options);
    await connection.commit;
  } catch (err) {
    res.send(error_format(err));
  } finally {
    if (connection) {
      await connection.close();
    }
    res.send(result);
  }
});

// DELETE API - basic delete using
// need to use POSTMAN, Thunder Client(vscode extension), or CURL(command line - recommend using git bash)
// for test, run twice using same parms to see how the output differs
app.route('/api/state/:state_code').delete(async function (req, res) {
  let connection, result, sql, options, binds;
  try {
    sql = `DELETE FROM template_states WHERE state_code = :state_code`;
    binds = {
      state_code: { type: oracledb.STRING, dir: oracledb.BIND_IN, val: req.params.state_code },
    };
    options = { autoCommit: true };

    connection = await oracledb.getConnection(dbConfig);

    result = await connection.execute(sql, binds, options);
    await connection.commit;
  } catch (err) {
    res.send(error_format(err));
  } finally {
    if (connection) {
      await connection.close();
    }
    res.send(result);
  }
});

// POST API using oracle package call
// TEST USING FOLLOWING JSON
// {"p_method":"u","p_state_code":"tN","p_user_id":"bueLleR"}
// REMOVE EXCEPTIONS FROM PLSQL TO SHOW WHAT HAPPENS
app.route('/api/state/pkg').post(async function (req, res) {
  let connection, result, sql, binds;
  try {
    connection = await oracledb.getConnection(dbConfig);
    sql = `BEGIN template.template_states_pkg.crud(p_method => :p_method, p_state_code => :p_state_code, p_user_id => :p_user_id, p_status => :p_status); END;`;

    binds = {
      p_method: { val: req.body.p_method, dir: oracledb.BIND_IN, type: oracledb.STRING },
      p_state_code: { val: req.body.p_state_code, dir: oracledb.BIND_IN, type: oracledb.STRING },
      p_user_id: { val: req.body.p_user_id, dir: oracledb.BIND_IN, type: oracledb.STRING },
      p_status: { dir: oracledb.BIND_OUT },
    };

    result = await connection.execute(sql, binds);
  } catch (err) {
    res.send(error_format(err));
  } finally {
    if (connection) {
      await connection.close();
    }
    res.send(result);
  }
});

function error_format(input) {
  let error_response;
  let error = input.toString().split(': ')[2].split(' ');
  if (input.errorNum === 12899) {
    error_response = 'Error: ' + [error[0], error[1], error[2]].join(' ');
  } else if (input.errorNum === 942) {
    error_response = 'Error: no records found';
  } else error_response = 'Error: ' + [error[0], error[1]].join(' ');
  return error_response;
}
